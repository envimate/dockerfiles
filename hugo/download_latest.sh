#!/usr/bin/env bash

REPO_NAME="gohugoio/hugo"
LATEST_RELEASE="$(curl -L -s -H 'Accept: application/json' https://github.com/${REPO_NAME}/releases/latest)"
LATEST_VERSION="$(echo $LATEST_RELEASE | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/' | tr -d "v")"
LATEST_DEB_ARTIFACT="https://github.com/${REPO_NAME}/releases/download/v${LATEST_VERSION}/hugo_${LATEST_VERSION}_Linux-64bit.deb"
wget ${LATEST_DEB_ARTIFACT} -O ./hugo/hugo.deb

echo ${LATEST_VERSION}